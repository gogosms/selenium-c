using System;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Selenium.Tests
{
    [TestFixture]
    public class SeleniumSpecialElementsTests
    {

        private ChromeDriver _webDriver;

        [SetUp]
        public void Setup()
        {
            _webDriver = new ChromeDriver();
        }

        [TearDown]
        public void TearDown()
        {
            _webDriver.Quit();
        }


        [Test]
        public void Must_Correctly_Use_input()
        {
            _webDriver.Navigate().GoToUrl("http://testing.todvachev.com/special-elements/text-input-field/");
            var expectedElement = _webDriver.FindElement(By.Name("username"));
            Assert.IsTrue(expectedElement.Displayed);
            expectedElement.SendKeys("Tests ....");
            Thread.Sleep(TimeSpan.FromSeconds(3));
            expectedElement.Clear();
            Thread.Sleep(TimeSpan.FromSeconds(2));
            expectedElement.SendKeys("Hello");
            Thread.Sleep(TimeSpan.FromSeconds(2));
            Assert.That(expectedElement.GetAttribute("value"), Is.EqualTo("Hello"));
            Assert.That(expectedElement.GetAttribute("maxlength"), Is.EqualTo("10"));

        }

        [Test]
        public void Must_Correctly_Use_Check_Button()
        {
            _webDriver.Navigate().GoToUrl("http://testing.todvachev.com/special-elements/check-button-test-3/");
            var expectedElement = _webDriver.FindElement(By.CssSelector("#post-33 > div > p:nth-child(8) > input[type=checkbox]:nth-child(1)"));
            Assert.IsNull(expectedElement.GetAttribute("checked"));
            expectedElement = _webDriver.FindElement(By.CssSelector("#post-33 > div > p:nth-child(8) > input[type=checkbox]:nth-child(3)"));
            Assert.IsNotNull(expectedElement.GetAttribute("checked"));
            Assert.That(expectedElement.GetAttribute("checked"), Is.EqualTo("true"));

            expectedElement = _webDriver.FindElement(By.CssSelector("#post-33 > div > p:nth-child(8) > input[type=checkbox]:nth-child(1)"));
            expectedElement.Click();
            Thread.Sleep(TimeSpan.FromSeconds(2));
            Assert.IsNotNull(expectedElement.GetAttribute("checked"));
            Assert.That(expectedElement.GetAttribute("checked"), Is.EqualTo("true"));
            Thread.Sleep(TimeSpan.FromSeconds(2));
        }

        [Test]
        public void Must_Correctly_Use_Radio_Button()
        {
            _webDriver.Navigate().GoToUrl("http://testing.todvachev.com/special-elements/radio-button-test/");
            var options = new[] { 1, 3, 5 };
            var expectedRadioElementMale = GetExpectedRadioElement(options[0]);
            Assert.That(expectedRadioElementMale.GetAttribute("checked"), Is.EqualTo("true"));

            var expectedRadioElementFemale = GetExpectedRadioElement(options[1]);
            Assert.IsNull(expectedRadioElementFemale.GetAttribute("checked"));

            var expectedRadioElementOther = GetExpectedRadioElement(options[2]);
            Assert.IsNull(expectedRadioElementOther.GetAttribute("checked"));
            Thread.Sleep(TimeSpan.FromSeconds(2));
            expectedRadioElementOther.Click();
            Assert.IsNull(expectedRadioElementFemale.GetAttribute("checked"));
            Assert.IsNull(expectedRadioElementMale.GetAttribute("checked"));
            Thread.Sleep(TimeSpan.FromSeconds(2));
        }


        [Test]
        public void Must_Correctly_Use_Alert()
        {
            _webDriver.Navigate().GoToUrl("http://testing.todorvachev.com/special-elements/alert-box/");
            var alert = _webDriver.SwitchTo().Alert();
            Assert.IsNotNull(alert);
            Assert.That(alert.Text, Is.EqualTo("Hello! I am Alert Box! Click \"OK\" to dismiss me!"));
            Thread.Sleep(TimeSpan.FromSeconds(2));
            alert.Accept();
            Thread.Sleep(TimeSpan.FromSeconds(2));
        }

        private IWebElement GetExpectedRadioElement(int optionValue)
        {
            return _webDriver.FindElement(By.CssSelector($"#post-10 > div > form > p:nth-child(6) > input[type=radio]:nth-child({optionValue})"));
        }
    }
}