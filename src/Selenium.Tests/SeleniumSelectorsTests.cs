using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Selenium.Tests
{
    [TestFixture]
    public class SeleniumSelectorsTests
    {
        private ChromeDriver _webDriver;

        [SetUp]
        public void Setup()
        {
            _webDriver = new ChromeDriver();
        }

        [TearDown]
        public void TearDown()
        {
            _webDriver.Quit();
        }

        
        [Test]
        public void Must_Correctly_FindElement_When_Use_By_Name_Selector()
        {
            _webDriver.Navigate().GoToUrl("http://testing.todvachev.com/selectors/name/");
            //Use By.Name
            var element = _webDriver.FindElement(By.Name("myName"));
            Assert.IsTrue(element.Displayed);
            Assert.Throws<NoSuchElementException>(() => _webDriver.FindElement(By.Name("WrongName")));
            element.SendKeys("Rodrigo");
            Thread.Sleep(TimeSpan.FromSeconds(1));
        }

        [Test]
        public void Must_Correctly_FindElement_When_Use_By_Id_Selector()
        {
            _webDriver.Navigate().GoToUrl("http://testing.todvachev.com/selectors/id/");
            var element = _webDriver.FindElement(By.Id("testImage"));
            Assert.IsTrue(element.Displayed);
            Assert.Throws<NoSuchElementException>(() => _webDriver.FindElement(By.Id("WrongName")));
            
            Thread.Sleep(TimeSpan.FromSeconds(1));
        }


        [Test]
        public void Must_Correctly_FindElement_When_Use_By_ClassName_Selector()
        {
            _webDriver.Navigate().GoToUrl("http://testing.todvachev.com/selectors/class-name/");
            var element = _webDriver.FindElement(By.ClassName("testClass"));
            Assert.IsTrue(element.Displayed);
            Assert.That(element.Text, Is.EqualTo("This is a paragraph with text that belongs to a class."));
            Assert.Throws<NoSuchElementException>(() => _webDriver.FindElement(By.ClassName("WrongName")));
            Thread.Sleep(TimeSpan.FromSeconds(1));
        }


        [Test]
        public void Must_Correctly_FindElement_When_Use_By_Css_Selector()
        {
            _webDriver.Navigate().GoToUrl("http://testing.todvachev.com/selectors/css-path/");
            var element = _webDriver.FindElement(By.CssSelector("#post-108 > div > figure > img"));
            Assert.IsTrue(element.Displayed);
            var elementXPath = _webDriver.FindElement(By.XPath("//*[@id=\"post-108\"]/div/figure/img"));
            Assert.IsTrue(elementXPath.Displayed);
            Assert.Throws<NoSuchElementException>(() => _webDriver.FindElement(By.CssSelector("#Wrong Element > WrongName")));
            Assert.Throws<NoSuchElementException>(() => _webDriver.FindElement(By.XPath("//*[@id=\"post-Wrong-Image\"]/div/figure/img")));
            Thread.Sleep(TimeSpan.FromSeconds(1));
        }

        
    }
}